package main

import (
  "fmt"
  "log"
  "net/http"
  "os"
)

func main() {
  events := make(chan map[string]interface{})

  go func() {
    for {
      e := <-events
      log.Printf("Received event: %+v\n", e)
    }
  }()

  pattern, ok := os.LookupEnv("PATTERN")
	if !ok {
		pattern = "/"
	}
	log.Printf("Registering URI pattern %s", pattern)
  http.HandleFunc(pattern, HandleHook(events, Context{}))

	port, ok := os.LookupEnv("PORT")
	if !ok {
		port = "8080"
	}
	log.Printf("Listening on port %s", port)
	http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
}
