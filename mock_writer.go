package main

import (
	"bytes"
	"net/http"
)

type ResponseWriter struct {
	Buffer       *bytes.Buffer
	ResponseCode int
}

func NewMockResponseWriter() *ResponseWriter {
	var buffer bytes.Buffer
	return &ResponseWriter{
		Buffer: &buffer,
	}
}

func (rw *ResponseWriter) Header() http.Header {
	return nil
}

func (rw *ResponseWriter) Write(b []byte) (int, error) {
	return rw.Buffer.Write(b)
}

func (rw *ResponseWriter) WriteHeader(responseCode int) {
	rw.ResponseCode = responseCode
}
